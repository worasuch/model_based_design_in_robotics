%% 

M = 3.15;
m = 0.251;
g = 9.80665;
beta = 0.00983;
b = 0.64;
l = 0.305;

A = [0 0 1 0;
    0 0 0 1;
    0 m*g/(M+m) (m-b)/(M+m) -m*beta/((M+m)*l);
    0 ((M+m)*g)/(M*l) -b/(M*l) -((beta/m*l*l)-(beta/(M*l*l)))];
B = [0;0;1/(M+m);1/(M*l)];
C = [1 0 0 0;
     0 0 1 0];
D = [0;
     0];
Q =[1 0 0 0;
    0 1 0 0;
    0 0 10 0;
    0 0 0 100];
R = 100;
K = lqr(A,B,Q,R)