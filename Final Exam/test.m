syms M m l g x x_d x_dd th th_d th_dd F z b beta;
%%%%%%%%%%%%%%%%%%%%%%%%%%55
% Dynamic Model
%x_dd = (F + (m*g*cos(th)*sin(th)) + (m*l*sin(th)*th_d*th_d))/(M + (m*sin(th)^2));
%th_dd = ( (g*sin(th)*(M+m)) + (F*cos(th)) - (m*l*sin(th)*cos(th)*th_d*th_d) )/( l*(M + (m*sin(th)^2)) );
% (2) in (1)
% x_dd = simplify(x_dd);
x_dd = (m*l*cos(th) - b*x_d - m*l*sin(th)*th_d^2 + F)/(M + m);
th_dd = (m*l*cos(th)*x_dd - beta*th_d + m*g*l*sin(th))/(m*(l^2));

A = jacobian([x_d;x_dd;th_d;th_dd],[x,x_d,th,th_d]);
B = jacobian([x_d;x_dd;th_d;th_dd],[F]);
C = [1 0 0 0;
     0 0 1 0];
D = [0;
     0];

% initial value
M = 3.15;
m = 0.251;
g = 9.80665;
beta = 0.00983;
b = 0.64;
%theta_0 = pi;%pi-pi/3;
l = 0.305;

th = 0;
th_d = 0;
th_dd = 0;
F = 0;

A = inline(A,'M','m','g','l','th','th_d','F');
A = A(M,m,g,l,th,th_d,F);

B = inline(B,'M','m','g','l','th','th_d','F');
B = B(M,m,g,l,th,th_d,F);

states = {'x' 'th' 'x_dot' 'th_d'};
inputs = {'F'};
outputs = {'x'; 'th'};
sys_ss = ss(A,B,C,D,'statename',states,'inputname',inputs,'outputname',outputs);

poles = eig(A);
co = ctrb(sys_ss);
controllability = rank(co);

Q = C'*C;
Q(1,1) = 50;
Q(3,3) = 100;
R = 100;
K = lqr(A,B,Q,R)

Ac = [(A-B*K)];
Bc = [B];
Cc = [C];
Dc = [D];

states = {'x' 'x_dot' 'th' 'th_d'};
inputs = {'F'};
outputs = {'x'; 'th'};
sys_cl = ss(Ac,Bc,Cc,Dc,'statename',states,'inputname',inputs,'outputname',outputs);
t = 0:0.01:5;
r =0.2*ones(size(t));
[y,t,x]=lsim(sys_cl,r,t);
[AX,H1,H2] = plotyy(t,y(:,1),t,y(:,2),'plot');
set(get(AX(1),'Ylabel'),'String','cart position (m)')
set(get(AX(2),'Ylabel'),'String','pendulum angle (radians)')
title('Step Response with LQR Control')