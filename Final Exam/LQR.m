%% Used this for calculating LQR in order to get K 

% params 
M = 3.15;
m = 0.251;
g = 9.80665;
beta = 0.00983;
b = 0.64;
l = 0.305;

% linearize around theta = 0
A = [0 0 1 0;
    0 0 0 1;
    0 (m*g)/M -b/M -beta/(M*l);
    0 ((m/M)+1)*(g/l) -b/(M*l) -((1/M)+(1/m))*(beta/(l*l))];
B = [0;0;1/(M);1/(M*l)];

% Q anr R matrices 
Q = [1 0 0 0;
    0 1 0 0;
    0 0 10 0;
    0 0 0 100];
R = 50;

% K is given by lqr function
K = lqr(A,B,Q,R)

% And used K metrice in simulink